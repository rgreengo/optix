import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

import { Patient } from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  url = 'http://localhost/optix/api/patient/';

  constructor(private httpClient: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type, JWT',
      'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
      'Cache-Control': 'private, max-age=1, no-cache, no-store, must-revalidate',
      // tslint:disable-next-line:object-literal-key-quotes
      'Pragma': 'public'
    })
  };

  // Obtem todos os patients
  getAllPatients(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.url)
      .pipe(
        map(result => result['data']),
        catchError(this.handleError));
  }

  getById(id: number): Observable<Patient> {
    return this.httpClient.get<Patient>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  updatePatient(obj: Patient): Observable<Patient> {
    console.log(`Service Patient data received name patient = ${obj.name}`);
    // tslint:disable-next-line:max-line-length
    // return this.httpClient.put<Patient>(this.url + obj.id, `patId=${obj.patId}&name=${obj.name}&sex=${obj.sex}&birthDate=21%2F10%2F1988&bloodGroup=${obj.bloodGroup}&cpf=${obj.cpf}&rg=${obj.rg}&email=${obj.email}&phoneMobile=${obj.phoneMobile}&placeOfBirth=${obj.placeOfBirth}`,
    // tslint:disable-next-line:max-line-length
    return this.httpClient.put<Patient>(this.url, `patId=33445566&name=MalucoAtualizado&sex=M&birthDate=01%2F04%2F2000&bloodGroup=O-&cpf=191.807.130-68&rg=778899652&email=fulano%40mail.com&phoneMobile=(51)+99988-7766&placeOfBirth=Porto+Alegre`,
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', 'application/json')
    }).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  savePatient(obj: Patient): Observable<Patient> {
    // tslint:disable-next-line:max-line-length
    // return this.httpClient.post<Patient>(this.url, `patId=${obj.patId}&name=${obj.name}&sex=${obj.sex}&birthDate=21%2F10%2F1988&bloodGroup=${obj.bloodGroup}&cpf=${obj.cpf}&rg=${obj.rg}&email=${obj.email}&phoneMobile=${obj.phoneMobile}&placeOfBirth=${obj.placeOfBirth}`,
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<Patient>(this.url, `patId=33445566&name=Maluco111&sex=M&birthDate=01%2F04%2F2000&bloodGroup=O-&cpf=191.807.130-68&rg=778899652&email=fulano%40mail.com&phoneMobile=(51)+99988-7766&placeOfBirth=Porto+Alegre`,
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', 'application/json')
    }).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  deletePatient(obj: Patient) {
    return this.httpClient.delete<Patient>(this.url + obj.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }


  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
