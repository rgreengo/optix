import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

import { Study } from './../models/study';


@Injectable({
  providedIn: 'root'
})
export class StudyService {

  url = 'http://localhost/optix/api/study/';

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type, JWT',
      'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
      'Cache-Control': 'private, max-age=1, no-cache, no-store, must-revalidate',
      // tslint:disable-next-line:object-literal-key-quotes
      'Pragma': 'public'
    })
  };

  getAllStudies(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.url)
      .pipe(
        map(result => result['data']),
        catchError(this.handleError));
  }

  getById(id: number): Observable<Study> {
    return this.httpClient.get<Study>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  updateStudy(obj: Study): Observable<Study> {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.put<Study>(this.url + obj.id, `patientFk=2&date=02%2F12%2F2019%2008%3A12%3A33&description=${obj.description}&modality=${obj.modality}&accNumber=${obj.accNumber}&refPhysician=${obj.refPhysician}&crmPhysician=${obj.crmPhysician}`,
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', 'application/json')
    }).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  saveStudy(obj: Study): Observable<Study> {
    // tslint:disable-next-line:max-line-length
    // return this.httpClient.post<Study>(this.url, `patientFk=2&date=02%2F12%2F2019%2008%3A12%3A33&description=${obj.description}&modality=${obj.modality}&accNumber=${obj.accNumber}&refPhysician=${obj.refPhysician}&crmPhysician=${obj.crmPhysician}`, this.httpOptions)
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<Study>(this.url, `patientFk=12&date=01%2F12%2F2019%2010%3A05%3A20&description=MalucoSemSolucao12&modality=CR&accNumber=112233&refPhysician=O%20M%C3%A9dico%20Solicitante&crmPhysician=2345`,
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept', 'application/json')
    }).pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  deleteStudy(obj: Study) {
    return this.httpClient.delete<Study>(this.url + obj.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }


  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
