import { HttpHeaders } from '@angular/common/http';

export class Config {
    url = 'http://localhost';
    httpOptions: any = {
        headers: new HttpHeaders({
           'Content-Type': 'application/json',
           'Access-Control-Allow-Origin': '*',
           'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type, JWT',
           'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
           'Cache-Control': 'private, max-age=1, no-cache, no-store, must-revalidate'
        })
    };
}
