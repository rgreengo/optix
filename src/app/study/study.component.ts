import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { StudyService } from './../services/study.service';
import { Study } from './../models/study';

@Component({
  selector: 'app-study',
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.css']
})
export class StudyComponent implements OnInit {

  study = {} as Study;
  studies: any = [];
  editProfileForm: FormGroup;
  closeResult = '';

  currentPage = 1;
  itemsPerPage = 5;
  pageSize: number;

  constructor(private studyService: StudyService,
              private fb: FormBuilder,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.editProfileForm = this.fb.group({
      id: [''],
      accNumber: [''],
      description: [''],
      crmPhysician: [''],
      date: [''],
      modality: [''],
      refPhysician: [''],
    });

    this.getAll();
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }

  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  getAll() {
    this.studyService.getAllStudies().subscribe(result => { this.studies = result; });
  }

  deleteStudy(obj: Study) {
    this.studyService.deleteStudy(obj).subscribe(() => {
      this.getAll();
    });
  }


  // tslint:disable-next-line:max-line-length
  editStudy(targetModal: any, patient: { id: any; name: any; email: any; cpf: any; rg: any; birthDate: any; bloodGroup: any; patId: any; phoneMobile: any; placeOfBirth: any; sex: any; }) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });

    // console.log('Edit item');
    // console.log(patient.cpf);

    this.editProfileForm.patchValue({
      id: patient.id,
      name: patient.name,
      email: patient.email,
      cpf: patient.cpf,
      rg: patient.rg,
      birthDate: patient.birthDate,
      bloodGroup: patient.bloodGroup,
      patId: patient.patId,
      phoneMobile: patient.phoneMobile,
      placeOfBirth: patient.placeOfBirth,
      sex: patient.sex
    });
  }


  onSave() {
    console.log('Submit button was clicked!');

    class StudyClass implements Study {
      id: number;
      accNumber: string;
      description: string;
      crmPhysician: string;
      date: string;
      modality: string;
      refPhysician: string;
      patientFk: string;
    }

    if (this.editProfileForm.getRawValue().id > 0) {

      const studyToUpdate = new StudyClass();
      studyToUpdate.id = this.editProfileForm.getRawValue().id;
      studyToUpdate.accNumber = this.editProfileForm.getRawValue().accNumber;
      studyToUpdate.description = this.editProfileForm.getRawValue().description;
      studyToUpdate.crmPhysician = this.editProfileForm.getRawValue().crmPhysician;
      studyToUpdate.modality = this.editProfileForm.getRawValue().modality;
      studyToUpdate.refPhysician = this.editProfileForm.getRawValue().refPhysician;

      this.studyService.updateStudy(studyToUpdate).subscribe(() => {
        this.getAll();
      });
    } else {
      const studyToUpdate = new StudyClass();
      studyToUpdate.accNumber = this.editProfileForm.getRawValue().accNumber;
      studyToUpdate.description = this.editProfileForm.getRawValue().description;
      studyToUpdate.crmPhysician = this.editProfileForm.getRawValue().crmPhysician;
      studyToUpdate.modality = this.editProfileForm.getRawValue().modality;
      studyToUpdate.refPhysician = this.editProfileForm.getRawValue().refPhysician;

      this.studyService.saveStudy(studyToUpdate).subscribe(() => {
        this.getAll();
      });
    }
    this.modalService.dismissAll();
    // console.log('res:', this.editProfileForm.getRawValue());
  }


  open(targetModal: any) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });
  }
}
