import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { PatientService } from './../services/patient.service';
import { Patient } from './../models/patient';



@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  editProfileForm: FormGroup;
  patient = {} as Patient;
  patients: any = [];
  closeResult = '';

  currentPage = 1;
  itemsPerPage = 5;
  pageSize: number;

  constructor(private patientService: PatientService,
              private fb: FormBuilder,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.editProfileForm = this.fb.group({
      id: [''],
      name: [''],
      email: [''],
      cpf: [''],
      rg: [''],
      birthDate: [''],
      bloodGroup: [''],
      patId: [''],
      phoneMobile: [''],
      placeOfBirth: [''],
      sex: ['']
    });
    this.getAll();
  }

  getAll() {
    this.patientService.getAllPatients().subscribe(result => { this.patients = result; });
  }

  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }

  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  deletePatient(obj: Patient) {
    this.patientService.deletePatient(obj).subscribe(() => {
      this.getAll();
    });
  }

  // tslint:disable-next-line:max-line-length
  editPatient(targetModal: any, patient: { id: any; name: any; email: any; cpf: any; rg: any; birthDate: any; bloodGroup: any; patId: any; phoneMobile: any; placeOfBirth: any; sex: any; }) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });

    // console.log('Edit item');
    // console.log(patient.cpf);

    this.editProfileForm.patchValue({
      id: patient.id,
      name: patient.name,
      email: patient.email,
      cpf: patient.cpf,
      rg: patient.rg,
      birthDate: patient.birthDate,
      bloodGroup: patient.bloodGroup,
      patId: patient.patId,
      phoneMobile: patient.phoneMobile,
      placeOfBirth: patient.placeOfBirth,
      sex: patient.sex
    });
  }


  onSave() {
    console.log('Submit button was clicked!');

    class PatientClass implements Patient {
      id: number;
      birthDate: string;
      bloodGroup: string;
      cpf: string;
      email: string;
      name: string;
      patId: string;
      phoneMobile: string;
      placeOfBirth: string;
      rg: string;
      sex: string;
    }

    if (this.editProfileForm.getRawValue().id > 0) {

      const patientToUpdate = new PatientClass();
      patientToUpdate.id = this.editProfileForm.getRawValue().id;
      patientToUpdate.birthDate = this.editProfileForm.getRawValue().birthDate;
      patientToUpdate.bloodGroup = this.editProfileForm.getRawValue().bloodGroup;
      patientToUpdate.cpf = this.editProfileForm.getRawValue().cpf;
      patientToUpdate.email = this.editProfileForm.getRawValue().email;
      patientToUpdate.name = this.editProfileForm.getRawValue().name;
      patientToUpdate.patId = this.editProfileForm.getRawValue().patId;
      patientToUpdate.phoneMobile = this.editProfileForm.getRawValue().phoneMobile;
      patientToUpdate.placeOfBirth = this.editProfileForm.getRawValue().placeOfBirth;
      patientToUpdate.rg = this.editProfileForm.getRawValue().rg;
      patientToUpdate.sex = this.editProfileForm.getRawValue().sex;

      this.patientService.updatePatient(patientToUpdate).subscribe(() => {
        this.getAll();
      });
      // console.log('Edit mode ID = ' + this.editProfileForm.getRawValue().id);
    } else {

      const patientToUpdate = new PatientClass();
      patientToUpdate.birthDate = this.editProfileForm.getRawValue().birthDate;
      patientToUpdate.bloodGroup = this.editProfileForm.getRawValue().bloodGroup;
      patientToUpdate.cpf = this.editProfileForm.getRawValue().cpf;
      patientToUpdate.email = this.editProfileForm.getRawValue().email;
      patientToUpdate.name = this.editProfileForm.getRawValue().name;
      patientToUpdate.patId = this.editProfileForm.getRawValue().patId;
      patientToUpdate.phoneMobile = this.editProfileForm.getRawValue().phoneMobile;
      patientToUpdate.placeOfBirth = this.editProfileForm.getRawValue().placeOfBirth;
      patientToUpdate.rg = this.editProfileForm.getRawValue().rg;
      patientToUpdate.sex = this.editProfileForm.getRawValue().sex;

      this.patientService.savePatient(patientToUpdate).subscribe(() => {
        this.getAll();
      });
    }
    this.modalService.dismissAll();
    // console.log('res:', this.editProfileForm.getRawValue());
  }


  // modal - methods

  open(targetModal: any) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `console: ${reason}`;
    }
  }



}
