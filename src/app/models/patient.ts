export interface Patient {
    id: number;
    birthDate: string;
    bloodGroup: string;
    cpf: string;
    email: string;
    name: string;
    patId: string;
    phoneMobile: string;
    placeOfBirth: string;
    rg: string;
    sex: string;
}
