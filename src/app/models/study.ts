export interface Study {
    id: number;
    accNumber: string;
    description: string;
    crmPhysician: string;
    date: string;
    modality: string;
    refPhysician: string;
    patientFk: string; // FK
}
